package de.opencharacter.utils;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class TestChangelog {

	@Test
	void test() {
		ArrayList<Version> changes = new ArrayList<>();

		// 020
		ArrayList<Entry> test = new ArrayList<>();
		test.add(new Entry("1", "test1"));
		test.add(new Entry("2", "test2"));
		changes.add(new Version("2019-04-18", "0.2.0", test));

		Changelog log = new Changelog(changes);

		for (Version v : log.getVersions()) {
			System.out.println(v.getVersion() + ", " + v.getRelease());
			for (Entry e : v.getEntries()) {
				System.out.println(e.getEntry() + " " + e.getChange());
			}
		}

	}

}
