<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <meta charset="UTF-8">
    <title>Open Character</title>
</head>
<body>
	<div class="container" id="welcome-container">
		<div class="jumbotron">
			
			<h1 class="display-4"><i class="fas fa-scroll" id="scroll"></i>Open Character</h1>
	  		<p class="lead">Welcome to the 'Dungeon &amp; Dragons 3.5' character generator of the
			<i>Open Character</i> Service! Here you can create your own characters. These characters can
			easily be used by DM's to populate their world. </p>
			<form action="edit" method="get">
   				<Button class="btn btn-info" type="submit"><i class="fas fa-plus"></i>Generate character</Button>
  			</form>
	  		<!-- <hr class="my-4">
	 		<a class="btn btn-primary btn-sm" href="#" role="button">Learn more</a>  -->
		</div>
		
		<div class="row">
	  		<div class="col-sm-4">
	  			<div class="card">
			  		<div class="card-body">
			    		<h5 class="card-title"><i class="fas fa-file-code"></i>Get involved</h5>
			    		<p class="card-text">
			    			This application uses the <a href="https://gitlab.com/FelixTaylor/GameMaster/">DDCharacter</a> Java 
				 			library to calculate and validate 'Dungeon &amp; Dragons 3.5' characters. <!-- The library is open source
				 			so you can use it in your own project are help developing it. -->
			    		</p>
			    		<a href="https://gitlab.com/FelixTaylor/GameMaster/" class="card-link">DDCharacter Project</a>
			 	 	</div>
				</div>
	  		</div>
	  		<div class="col-sm-4">
	  			<div class="card">
			  		<div class="card-body">
			    		<h5 class="card-title"><i class="fas fa-exclamation-circle"></i>Thats new!</h5>
			    		<p class="card-text">
			    			Go to the <a href="changelog">changelog</a> page and read about every change that was made regarding the 'Open Character'
			    			project.
			    		</p>
			    		<a href="changelog" class="card-link">Changelog</a>
			 	 	</div>
				</div>
	  		</div>
	  		<div class="col-sm-4">
		  		<div class="card">
			  		<div class="card-body">
			    		<h5 class="card-title"><i class="fas fa-bug"></i>Found a bug?</h5>
			    		<p class="card-text">
							Please let me know and send me an email! Try to be as specific as you can to describe the error you
							have found.
			    		</p>
			    		<a href="mailto:incoming+felixtaylor-ddcharacterweb-11831191-issue-@incoming.gitlab.com?" class="card-link">issue@opencharacter</a>
			 	 	</div>
				</div>
	  		</div>
  		</div>
	</div>
	
</body>
<script src="js/jquery-3.4.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
</html>
