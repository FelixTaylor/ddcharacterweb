var abilities = {
	"strength": 0,
	"dexterity": 0,
	"constitution": 0,
	"intelligence": 0,
	"wisdom": 0,
	"charisma": 0
};

$(document).ready(function() {
	
    // Random race checkbox
    $("#random-race").click(function() {
    	if (this.checked === true) {
    		$("#char-race").attr("disabled", "disabled");
    	} else {
    		$("#char-race").removeAttr("disabled");
    	}
    });

    // Random clazz checkbox
    $("#random-clazz").click(function() {
    	if (this.checked === true) {
    		$("#char-clazz").attr("disabled", "disabled");
    	} else {
    		$("#char-clazz").removeAttr("disabled");
    	}
    });
    
    // Random armor checkbox
    $("#random-armor").click(function() {
    	if (this.checked === true) {
    		$("#char-armor").attr("disabled", "disabled");
    	} else {
    		$("#char-armor").removeAttr("disabled");
    	}
    });
    
    // Random weapon checkbox
    $("#random-weapon").click(function() {
    	if (this.checked === true) {
    		$("#char-weapon").attr("disabled", "disabled");
    	} else {
    		$("#char-weapon").removeAttr("disabled");
    	}
    });
    
    // Random disposition checkbox
    $("#random-disposition").click(function() {
    	if (this.checked === true) {
    		$("#char-attitude").attr("disabled", "disabled");
    		$("#char-alignment").attr("disabled", "disabled");
    		$("#char-deity").attr("disabled", "disabled");
    	} else {
    		$("#char-attitude").removeAttr("disabled");
    		$("#char-alignment").removeAttr("disabled");
    		$("#char-deity").removeAttr("disabled");
    	}
    });
    	
    $(function () {
	  $("[data-toggle='tooltip']").tooltip()
	})
    
	$(".char-ability").on("input", function() {
		abilities[this.id] = Number(this.value);
		let sum = abilities["strength"] + 
			abilities["dexterity"]      + 
			abilities["constitution"]   + 
			abilities["intelligence"]   + 
			abilities["wisdom"]         + 
			abilities["charisma"];
		
		$("#used-ability-points").html(sum);
	});
    
    console.log("ready!");
});
