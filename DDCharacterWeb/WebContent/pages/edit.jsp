<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <meta charset="UTF-8">
    <title>Open Character - Edit</title>
</head>
<body>
	<c:import url="/components/header.html"/>
	<div class="container">
		<form action="view" method="post">
		  	<div class="row">
		  		<div class="col-sm-12">
	    			<div class="row">
						<div class="col-sm-6">
							<table class="table table-striped">
					   			<tbody>
					   				<thead class="thead-light">
										<tr>
											<th>Name</th>
											<th>
												<span style="float:right;">Level</span>
											</th>
										</tr>
									</thead>
									<tr>
										<td>
											<input type="text" name="char-name" id="name" placeholder="Name" class="form-control"
							    				data-toggle="tooltip" data-placement="bottom" title="The name of the character."
							    				value="${npc.getName()}"/>
							    		</td>
										<td>
											<input type="number" name="char-level" id="level" placeholder="Level" min="1" max="99" data-toggle="tooltip"
												data-placement="bottom" title="The level of the character. The default range of the level is between 1 and 20."
												value="${npc.getLevel()}"/>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-sm-6">
							<div class="alert alert-info" role="alert">
								<p>
						   			<i class="fas fa-info" id="generation-info-icon"></i><b>Random Character:</b>
						   			To generate a fully random character including the race, class, abilities, level,
						   			deity and so on, use click here:
					   			</p>
					   			<button type="button" class="btn-primary btn-lg btn-block">
					   				<i class="fas fa-dice"></i>
					   				Random
				   				</button>
							  	<!-- To generate a random character make sure to check the 'random-checkbox's' 
							  	at the race and class dropdown in the tribe section and the dropdown in the
							  	disposition section. The other field are generated randomly if no input was given. -->
							</div>
						</div>
					</div>
    			</div>
			</div>
			<div class="row">
		    	<div class="col-sm-6">
		    		<div class="row">
						<div class="col-sm-12">
							<table class="table table-striped">
					   			<tbody>
					   				<thead class="thead-light">
										<tr>
											<th>Tribe</th>
											<th>
												<span class="floatRight">
													<i class="fas fa-exclamation"></i>
												</span>
											</th>
										</tr>						
									</thead>
									<tr>
										<td>Race</td>
										<td>
											<select name="char-race" id="char-race">
												<c:forEach items="${races}" var="race">
											    	<option value="${race.getName()}" ${npc.getRace().getName() == race.getName() ? 'selected="selected"' : ''}>${race.getName()}</option>
											    </c:forEach>
											</select>
											<br />
											<span class="select-random text-muted">
											  	<input type="checkbox" class="form-check-input" id="random-race" name="random-race">
										    	<label class="form-check-label" for="random-race" data-toggle="tooltip"
										    		data-placement="bottom" title="Check this 'random' checkbox to select a race at random.">Random</label>
											</span>
										</td>
									</tr>
									<tr>
										<td>Class</td>
										<td>
											<select name="char-clazz" id="char-clazz">
												<c:forEach items="${clazzes}" var="clazz">
											    	<option value="${clazz.getName()}" ${npc.getClazz().getName() == clazz.getName() ? 'selected="selected"' : ''}>${clazz.getName()}</option>
											    </c:forEach>
											</select>
											<span class="select-random text-muted">
												<input type="checkbox" class="form-check-input" id="random-clazz" name="random-clazz">
										    	<label class="form-check-label" for="random-clazz" data-toggle="tooltip"
										    		data-placement="bottom" title="Check this 'random' checkbox to select a class at random.">Random</label>
								    		</span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
		    	<div class="col-sm-6">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-striped">
					   			<tbody>
					   				<thead class="thead-light">
										<tr>
											<th>
												<span data-toggle="tooltip" data-placement="right"
													title="The 'Disposition' determines the alignment and attitude of the character and if his actions are more evil or more good.">
														Disposition
												</span>
											</th>
											<th>
											<span class="select-random text-muted">
											  	<input type="checkbox" class="form-check-input" id="random-disposition" name="random-disposition">
										    	<label class="form-check-label" for="random-disposition" data-toggle="tooltip"
										    		data-placement="bottom" title="Check this 'random' checkbox to select a disposition including the deity, alignment and attitude at random.">Random</label>
											</span>
											</th>
										</tr>
									</thead>
									<tr>
										<td>
											<label for="char-deity" data-toggle="tooltip" data-placement="right"
							  					title="The deity the character beleves in.">
							  					Deity
							  				</label>
										</td>
										<td>
											<select name="char-deity" id="char-deity">
												<c:forEach items="${deities}" var="deity">
											    	<option value="${deity}" ${npc.getDeity().getName() == deity.getName() ? 'selected="selected"' : ''}>
											    		${deity.getName()}
									    			</option>
											    </c:forEach>
											</select>
										</td>
									</tr>
									<tr>
										<td>
											<label for="char-alignment" data-toggle="tooltip" data-placement="top"
							   					title="The alignment of the character. The two values alignment and attitude are combined to the disposition value. This value should correspond to the characters deity.">
							   						Alignment
							  				</label>
										</td>
										<td>
											<select name="char-alignment" id="char-alignment">
												<c:forEach items="${alignments}" var="alignment">
											    	<option value="${alignment}" ${npc.getAlignment().getName() == alignment.getName() ? 'selected="selected"' : ''}>${alignment.getName()}</option>
											    </c:forEach>
											</select>
										</td>
									</tr>
									<tr>
										<td>
											<label for="char-attitude" data-toggle="tooltip" data-placement="top"
								   				title="The attitude of the character. The two values alignment and attitude are combined to the disposition value. This value should correspond to the characters deity.">
								   					Attitude
							  				</label>
					  					</td>
										<td>
											<select name="char-attitude" id="char-attitude">
												<c:forEach items="${attitudes}" var="attitude">
											    	<option value="${attitude}" ${npc.getAttitude().getName() == attitude.getName() ? 'selected="selected"' : ''}>${attitude.getName()}</option>
											    </c:forEach>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
		    	<div class="col-sm-6">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-striped">
					   			<tbody>
					   				<thead class="thead-light">
										<tr>
											<th>
												<span data-toggle="tooltip" data-placement="right"
													title="The 'Abilities' are a crucial part of the character generation.">
														Abilities
												</span>
											</th>
											<th>
												<span class="floatRight">
													<i class="fas fa-exclamation"></i>
												</span>
											</th>
										</tr>
									</thead>
									<tr>
										<td>
											<label for="stength" data-toggle="tooltip" data-placement="right"
								    			title="The strength of the character determines the overall combat capabilities of the character and skills like carray capacity.">
								    				Strength
							    			</label>
					    				</td>
										<td>
											<input type="number" class="char-ability" name="char-str" id="strength" placeholder="STR" maxlength="3" min="1" data-toggle="tooltip" data-placement="left"
							    				title="An average skill value for a character is between 10 and 15." value="${npc.getAbility('str')}"/>
						    			</td>
									</tr>
									<tr>
										<td>
											<label for="dexterity" data-toggle="tooltip" data-placement="right"
					    						title="The dexterity of the character deterimes the overall agility of the character while in combat or while using skills like climbing.">
					    							Dexterity
					   						</label>
					    				</td>
										<td>
											<input type="number" class="char-ability" name="char-dex" id="dexterity" placeholder="DEX" maxlength="3" min="1" data-toggle="tooltip" data-placement="left"
					    						title="An average skill value for a character is between 10 and 15." value="${npc.getAbility('dex')}"/>
					    				</td>
									</tr>
									<tr>
										<td>
											<label for="constitution" data-toggle="tooltip" data-placement="right"
						    					title="The constitution of the character determines the overall combat resistence of the character and the skill concentration which is used while conjuring.">
						    						Constitution
					    					</label>
						    			</td>
										<td>
											<input type="number" class="char-ability" name="char-con" id="constitution" placeholder="CON" maxlength="3" min="1" data-toggle="tooltip" data-placement="left"
							    				title="An average skill value for a character is between 10 and 15." value="${npc.getAbility('con')}"/>
						    			</td>
									</tr>
									<tr>
										<td>
											<label for="intelligence" data-toggle="tooltip" data-placement="right"
					    						title="The intelligence of the character deterimes how smart the character can react in various situtations.">
					    							Intelligence
					   						</label>
					    				</td>
										<td>
											<input type="number" class="char-ability" name="char-int" id="intelligence" placeholder="INT" maxlength="3" min="1" data-toggle="tooltip" data-placement="left"
					    						title="An average skill value for a character is between 10 and 15." value="${npc.getAbility('int')}"/>
					    				</td>
				    				</tr>
				    				<tr>
				    					<td>
											<label for="wisdom" data-toggle="tooltip" data-placement="right"
					    						title="The wisdom of the character determines how good the character knows about variouse things how good he can remember specific situtations.">
					    							Wisdom
											</label>
					    				</td>
										<td>
											<input type="number" class="char-ability" name="char-wis" id="wisdom" placeholder="WIS" maxlength="3" min="1"  data-toggle="tooltip" data-placement="left"
					    						title="An average skill value for a character is between 10 and 15." value="${npc.getAbility('wis')}"/>
					    				</td>
				    				</tr>
				    				<tr>
				    					<td>
					    					<label for="charisma" data-toggle="tooltip" data-placement="right"
						    					title="The charisma of the character determines the overall ability to cooperate with other characters, the power of skills like persuading or trading.">
						    						Charisma
					    					</label>
						    			</td>
										<td>
											<input type="number" class="char-ability" name="char-cha" id="charisma" placeholder="CHA" maxlength="3" min="1" data-toggle="tooltip" data-placement="left"
						    					title="An average skill value for a character is between 10 and 15." value="${npc.getAbility('cha')}"/>
						    			</td>
					    			</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
		    		<div class="row">
						<div class="col-sm-12">
							<table class="table table-striped">
					   			<tbody>
					   				<thead class="thead-light">
										<tr>
											<th colspan="2">Equipment</th>
										</tr>						
									</thead>
									<tr>
										<td>Weapon</td>
										<td>
											<select name="char-weapon" id="char-weapon">
												<c:forEach items="${weapons}" var="weapon">
											    	<option value="${weapon}" ${npc.getWeapon().getName() == weapon.getName() ? 'selected="selected"' : ''}>${weapon.getName()}</option>
											    </c:forEach>
											</select>
											<br />
											<span class="select-random text-muted">
											  	<input type="checkbox" class="form-check-input" id="random-weapon" name="random-weapon">
										    	<label class="form-check-label" for="random-weapon" data-toggle="tooltip"
										    		data-placement="bottom" title="Check this 'random' checkbox to select a weapon at random.">Random</label>
											</span>
										</td>
									</tr>
									<tr>
										<td>Armor</td>
										<td>
											<select name="char-armor" id="char-armor">
												<c:forEach items="${armors}" var="armor">
											    	<option value="${armor}" ${npc.getArmor().getName() == armor.getName() ? 'selected="selected"' : ''}>${armor.getName()}</option>
											    </c:forEach>
											</select>
											<span class="select-random text-muted">
												<input type="checkbox" class="form-check-input" id="random-armor" name="random-armor">
										    	<label class="form-check-label" for="random-armor" data-toggle="tooltip"
										    		data-placement="bottom" title="Check this 'random' checkbox to select an armor at random.">Random</label>
								    		</span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-striped">
					   			<tbody>
					   				<thead class="thead-light">
										<tr>
											<th>Lore</th>
										</tr>
									</thead>
									<tr>
										<td>
											<textarea class="form-control" name="char-lore" rows="13">${npc.getLore()}</textarea>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
  			<!-- <Button class="btn btn-danger" name="action" value="clear"><i class="fas fa-skull-crossbones"></i>Clear</Button> -->
			<Button class="btn btn-success" type="submit" name="action" value="generate"><i class="fas fa-dice-d20"></i>Generate</Button>
		</form>
		<c:import url="/components/footer.html"/>
	</div>
</body>
<script src="js/jquery-3.4.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
</html>
