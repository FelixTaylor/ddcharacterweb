<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
	<meta charset="UTF-8">
	<title>Open Character Changelog</title>
</head>
<body>
	<c:import url="/components/header.html"/>
	<div class="container">
		<div class="row">
	    	<div class="col-sm-12">
	   			<table class="table table-striped">
		   			<tbody>
				  		<c:forEach items="${changelog.getVersions()}" var="version">
			   				<thead class="thead-dark">
								<tr>
									<th scope="col">${version.getVersion()}</th>
									<th scope="col">
										<span style="float:right;">
											${version.getRelease()}
										</span>
									</th>
							    </tr>
						  	</thead>
					   		<tr>
					   			<td colspan="2">
						   			<table class="table">
							   			<tbody>
									    	<c:forEach items="${version.getEntries()}" var="version">
									    	<tr>
									    		<td style="width:4rem;">
									    			<span class="badge badge-info">
									    				${version.getEntry()}
								    				</span>
							    				</td>
									    		<td>${version.getChange()}</td>
									    	</tr>
									    	</c:forEach>
								    	</tbody>
							    	</table>
					   			</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<c:import url="/components/footer.html"/>
	</div>
</body>
<script type="javascript" src="../js/jquery-3.4.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script type="javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</html>