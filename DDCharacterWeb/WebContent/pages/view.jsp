<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <meta charset="UTF-8">
<title>Open Character - ${npc.getName()}</title>
</head>
<body>
	<c:import url="/components/header.html"/>
    <div class="container">
    	<div class="row">
    		<div class="col-sm-12">
    			<h1>
	    			<b>${npc.getName()}</b>
	    		</h1>
	    		<p class="item-label text-muted">
   					Name
				</p>
    		</div>
   		</div>
    	<div class="row">
	    	<div class="col-sm-6">
	    		<div class="row">
	    			<div class="col-sm-4">
	    				<h4>
	    					<b>${npc.getRace().getName()}</b>
    					</h4>
    					<p class="item-label text-muted">
    						Race
   						</p>
	    			</div>
	    			<div class="col-sm-5">
	    				<h4>
	    					<b>${npc.getClazz().getName()}</b>
    					</h4>
    					<p class="item-label text-muted">
    						Class
   						</p>
	    			</div>
	    			<div class="col-sm-3">
	    				<h4>
	    					<b>${npc.getLevel()}</b>
    					</h4>
    					<p class="item-label text-muted">
    						Level
   						</p>
	    			</div>
	    		</div>
	    	</div>
	    	<div class="col-sm-6">
	    		<div class="row">
	    			<div class="col-sm-5">
	    				<h4 data-toggle="tooltip" data-placement="top" title="${npc.getDeity().getDescription()}">
	    					<b>${npc.getDeity().getName()}</b>
    					</h4>
    					<p class="item-label text-muted">
    						Deity
   						</p>
	    			</div>
	    			<div class="col-sm-4">
	    				<h4>
	    					<b>${npc.getAlignment().getName()}</b>
    					</h4>
    					<p class="item-label text-muted">
    						Alignment
   						</p>
	    			</div>
	    			<div class="col-sm-3">
	    				<h4>
	    					<b>${npc.getAttitude().getName()}</b>
    					</h4>
    					<p class="item-label text-muted">
    						Attitude
   						</p>
	    			</div>
	    		</div>
	    	</div>
    	</div>
    	<div class="row">
	    	<div class="col-sm-6">
    			<table class="table table-striped">
				  	<tbody>
				  		<thead class="thead-light">
							<tr>
								<th scope="col">Name</th>
						      	<th scope="col">Value</th>
						      	<th scope="col">Mod</th>
						    </tr>
					  	</thead>
		    			<c:forEach items="${npc.getAbilities()}" var="abilities">
				    		<tr>
								<td scope="row">${abilities.getName()}</td>
						     	<td>${abilities.getValue()}</td>
						     	<td>${abilities.getMod()}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
	    	</div>
	    	<div class="col-sm-6">
	    		<table class="table table-striped">
				  	<tbody>	
					  	<tr>
							<th scope="col">Hit points</th>
					      	<td scope="col">${npc.getMaxHitPoints()}</td>
					    </tr>
					    <tr data-toggle="tooltip" data-placement="right"
				    		title="The armor class value is the result of the combination of the armor value and the characters dexterity value.">
							<th scope="col">Armor class</th>
					      	<td scope="col">${totalArmor}</td>
					    </tr>
					    <tr>
							<th scope="col">Talent points</th>
					      	<td scope="col">${npc.getTalentPoints()}</td>
					    </tr>
					    <tr>
							<th scope="col">Skill points</th>
					      	<td scope="col">${npc.getSkillPoints()}</td>
					    </tr>
					</tbody>
				</table>
	    	</div>
    	</div>
    	<div class="row">
	    	<div class="col-sm-6">
    			<table class="table table-striped">
				  	<tbody>
				  		<thead class="thead-light">
							<tr>
								<th scope="col">Melee Attack</th>
						      	<th scope="col">Range Attack</th>
						      	<th scope="col">Base Attack Bonus</th>
						    </tr>
					  	</thead>
					  	<tr>
							<td scope="col">${meleeDamage}</td>
					      	<td scope="col">${rangeDamage}</td>
					      	<td scope="col">${npc.getBaseAttackBonus()}</td>
					    </tr>
					</tbody>
				</table>
	    	</div>
    	</div>
    	<div class="row">
	    	<div class="col-sm-6">
    			<table class="table table-striped">
				  	<tbody>
				  		<thead class="thead-light">
							<tr>
								<th scope="col">Fortitude</th>
						      	<th scope="col">Reflex</th>
						      	<th scope="col">Will</th>
						    </tr>
					  	</thead>
					  	<tr>
							<td scope="col">${fortitude}</td>
					      	<td scope="col">${reflex}</td>
					      	<td scope="col">${will}</td>
					    </tr>
					</tbody>
				</table>
	    	</div>
    	</div>
    	<div class="row">
	    	<div class="col-sm-6">
    			<table class="table table-striped">
				  	<tbody>
				  		<thead class="thead-light">
							<tr>
								<th scope="col">Weapon</th>
						      	<th scope="col">Type</th>
						      	<th scope="col">Damage</th>
						    </tr>
					  	</thead>
					  	<tr>
							<td scope="col">${npc.getWeapon().getName()}</td>
					      	<td scope="col">${npc.getWeapon().getType().getType()}</td>
					      	<td scope="col">
					      		${npc.getWeapon().getDiceRepresentation().getAmount()}D${npc.getWeapon().getDiceRepresentation().getRange()}
					      	</td>
					    </tr>
					</tbody>
				</table>
	    	</div>
    	</div>
    	<div class="row">
	    	<div class="col-sm-6">
    			<table class="table table-striped">
				  	<tbody>
				  		<thead class="thead-light">
							<tr>
								<th scope="col">Armor</th>
						      	<th scope="col">Value</th>
						    </tr>
					  	</thead>
					  	<tr>
					  		<td scope="col">${npc.getArmor().getName()}</td>
					      	<td scope="col">${npc.getArmor().getValue()}</td>
						</tr>
					</tbody>
				</table>
	    	</div>
    	</div>
    	<div class="row">
	    	<div class="col-sm-6">
    			<table class="table table-striped">
				  	<tbody>
				  		<thead class="thead-light">
							<tr>
								<th scope="col">Currency</th>
						      	<th scope="col">Amount</th>
						    </tr>
					  	</thead>
					  	<tr>
					      	<td scope="col">${npc.getCapital().getCurrency()}</td>
					      	<td scope="col">${npc.getCapital().getCapital()}</td>
					</tbody>
				</table>
	    	</div>
    	</div>
    	<c:if test="${not empty npc.getLore().trim()}">
		    <div class="row">
		    	<div class="col-sm-12">
	    			<table class="table table-striped">
					  	<tbody>
					  		<thead class="thead-light">
								<tr>
									<th scope="col">Lore</th>
							    </tr>
						  	</thead>
						  	<tr>
						      	<td scope="col">${npc.getLore()}</td>
						</tbody>
					</table>
		    	</div>
	    	</div>
		</c:if>
    	<div class="row">
    		<div class="col-sm-12">
    			<hr class="my-4">
    			<form action="edit" method="get">
    				<Button class="btn btn-info" type="submit"><i class="fas fa-edit"></i>Edit</Button>
   				</form>
    			<small class="form-text text-muted">${npc.getUUID()}</small>
    		</div>
   		</div>
   		<c:import url="/components/footer.html"/>
    </div>
</body>
<script src="js/jquery-3.4.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
</html>
