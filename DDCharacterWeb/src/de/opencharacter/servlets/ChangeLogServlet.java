package de.opencharacter.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.opencharacter.utils.Changelog;
import de.opencharacter.utils.Entry;
import de.opencharacter.utils.Version;

@WebServlet("/changelog")
public class ChangeLogServlet extends HttpServlet {
	private static final long serialVersionUID = -9170748792534359273L;
	private static final String CODE = "OCS-";

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ArrayList<Version> changes = new ArrayList<>();

		// 020
		ArrayList<Entry> _0_2_0 = new ArrayList<>();
		_0_2_0.add(new Entry(CODE + "X", "Improved character creation and view page"));
		_0_2_0.add(new Entry(CODE + "X", "Implemented Changelog"));
		_0_2_0.add(new Entry(CODE + "X", "Implemented welcome screen"));
		_0_2_0.add(new Entry(CODE + "X",
				"The character is now stored in the session so it can be used by the creation and view page"));
		changes.add(new Version("2019-04-XX", "0.2.0", _0_2_0));

		// 010
		ArrayList<Entry> _0_1_0 = new ArrayList<>();
		_0_1_0.add(new Entry(CODE + "X", "Project init"));
		changes.add(new Version("2019-04-13", "0.1.0", _0_1_0));

		request.setAttribute("changelog", new Changelog(changes));
		request.getRequestDispatcher("pages/changelog.jsp").forward(request, response);
	}
}
