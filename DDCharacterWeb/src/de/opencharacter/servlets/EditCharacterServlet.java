package de.opencharacter.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import character.base.Disposition;
import character.components.Clazz;
import character.components.Deity;
import character.components.Race;
import equipment.Armor;
import equipment.Weapon;

@WebServlet("/edit")
public class EditCharacterServlet extends HttpServlet {
	private static final long serialVersionUID = 1260276933457826899L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("/pages/edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/pages/edit.jsp").forward(request, response);
	}

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setAttribute("races", Race.values());
		request.setAttribute("clazzes", Clazz.values());
		request.setAttribute("deities", Deity.values());
		request.setAttribute("alignments", Disposition.Alignment.values());
		request.setAttribute("attitudes", Disposition.Attitude.values());

		request.setAttribute("weapons", Weapon.values());
		request.setAttribute("armors", Armor.values());
		request.getRequestDispatcher("/pages/edit.jsp").forward(request, response);

	}

}
