package de.opencharacter.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import character.NPC;
import character.base.Disposition;
import character.base.SavingThrows;
import character.components.Clazz;
import character.components.Deity;
import character.components.Race;
import equipment.Armor;
import equipment.Weapon;
import utils.Utils;

@WebServlet("/view")
public class ViewCharacterServlet extends HttpServlet {
	private static final long serialVersionUID = Utils.nextVersionUID();
	private NPC npc;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		this.npc = new NPC();
		this.npc.random();
		// Do I really need this call here?
		this.npc.removeTribeBonusFromAbilities();

		setRace(request);
		setClazz(request);

		setName(request.getParameter("char-name"));
		setLevel(request.getParameter("char-level"));

		setAbility("str", request.getParameter("char-str"));
		setAbility("dex", request.getParameter("char-dex"));
		setAbility("con", request.getParameter("char-con"));
		setAbility("int", request.getParameter("char-int"));
		setAbility("wis", request.getParameter("char-wis"));
		setAbility("cha", request.getParameter("char-cha"));

		setLore(request.getParameter("char-lore"));
		setDeity(request);
		setDisposition(request);

		setWeapon(request);
		setArmor(request);

		this.npc.distributeAbilities();
		this.npc.addTribeBonusToAbilities();
		this.npc.build();

		request.setAttribute("creationtime", Utils.dateToString("yyyy-MM-dd", this.npc.getCreationDate()));
		request.setAttribute("totalArmor", (this.npc.getArmor().getValue() + this.npc.getAbilityMod("dex")));

		setAttacks(request);

		// One of these methods throws a NullPointerException when used in servlet.
		request.setAttribute("fortitude", this.npc.getSavingThrow(SavingThrows.FORTITUDE));
		request.setAttribute("reflex", this.npc.getSavingThrow(SavingThrows.REFLEX));
		request.setAttribute("will", this.npc.getSavingThrow(SavingThrows.WILL));

		if (request.getSession().getAttribute("npc") != null) {
			request.getSession().removeAttribute("npc");
		}
		request.getSession().setAttribute("npc", this.npc);
		request.getRequestDispatcher("/pages/view.jsp").forward(request, response);

	}

	private void setAttacks(HttpServletRequest request) {
		int attacks = this.npc.getAttacks();
		String meleeDisplay = "";
		String rangeDisplay = "";

		if (attacks > 1) {
			int newMelee = this.npc.getMeleeMod();
			int newRange = this.npc.getRangeMod();
			meleeDisplay = Integer.toString(newMelee) + "/";
			rangeDisplay = Integer.toString(newRange) + "/";

			for (int i = 1; i < attacks; i++) {
				// TODO: Is this calculation correct?
				newMelee = newMelee - 5;
				newRange = newRange - 5;

				meleeDisplay += newMelee + (i == attacks - 1 ? "" : "/");
				rangeDisplay += newRange + (i == attacks - 1 ? "" : "/");
			}

		} else {
			meleeDisplay = Integer.toString(this.npc.getMeleeMod());
			rangeDisplay = Integer.toString(this.npc.getRangeMod());
		}

		request.setAttribute("meleeDamage", meleeDisplay);
		request.setAttribute("rangeDamage", rangeDisplay);
	}

	private void setDisposition(HttpServletRequest request) {
		if (request.getParameterValues("random-disposition") == null
				|| !request.getParameterValues("random-disposition")[0].equals("on")) {

			Disposition.Alignment alignment = Disposition.Alignment
					.valueOf(request.getParameter("char-alignment").toUpperCase());
			Disposition.Attitude attitude = Disposition.Attitude
					.valueOf(request.getParameter("char-attitude").toUpperCase());

			this.npc.setDisposition(alignment, attitude);
		}
	}

	private void setWeapon(HttpServletRequest request) {
		if (request.getParameterValues("random-weapon") == null
				|| !request.getParameterValues("random-weapon")[0].equals("on")) {
			this.npc.set(Weapon.valueOf(request.getParameter("char-weapon").toUpperCase()));
		}
	}

	private void setArmor(HttpServletRequest request) {
		if (request.getParameterValues("random-armor") == null
				|| !request.getParameterValues("random-armor")[0].equals("on")) {
			this.npc.set(Armor.valueOf(request.getParameter("char-armor").toUpperCase()));
		}
	}

	private void setAbility(String ability, String value) {
		if (!value.trim().isEmpty()) {
			this.npc.setAbility(ability, Integer.valueOf(value));
		}
	}

	private void setRace(HttpServletRequest request) {
		if (request.getParameterValues("random-race") == null
				|| !request.getParameterValues("random-race")[0].equals("on")) {
			this.npc.set(Race.valueOf(request.getParameter("char-race").toUpperCase()));
		}
	}

	private void setClazz(HttpServletRequest request) {
		if (request.getParameterValues("random-clazz") == null
				|| !request.getParameterValues("random-clazz")[0].equals("on")) {
			this.npc.set(Clazz.valueOf(request.getParameter("char-clazz").toUpperCase()));
		}
	}

	private void setDeity(HttpServletRequest request) {
		if (request.getParameterValues("random-disposition") == null
				|| !request.getParameterValues("random-disposition")[0].equals("on")) {
			this.npc.set(Deity.valueOf(request.getParameter("char-deity").replaceAll("\\s+", "_").toUpperCase()));
		}
	}

	private void setName(String name) {
		if (name.trim().isEmpty()) {
			this.npc.random("name");
		} else {
			// TODO: I have to escape html chars
			this.npc.setName(name);
		}
	}

	private void setLevel(String level) {
		if (!level.isEmpty()) {
			this.npc.setLevel(Integer.valueOf(level));
		}
	}

	private void setLore(String lore) {
		if (lore != null && !lore.trim().isEmpty()) {
			// TODO: I have to escape html chars
			this.npc.setLore(lore);
		}
	}
}