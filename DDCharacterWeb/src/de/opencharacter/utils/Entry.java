package de.opencharacter.utils;

public class Entry {

	private String entry;
	private String change;

	public Entry(String entry, String change) {
		super();
		this.entry = entry;
		this.change = change;
	}

	public String getEntry() {
		return this.entry;
	}

	public String getChange() {
		return this.change;
	}

}
