package de.opencharacter.utils;

import java.util.ArrayList;

public class Version {

	private String release;
	private String version;
	private ArrayList<Entry> entries;

	public Version() {
		this.release = "";
		this.version = "";
		this.entries = new ArrayList<Entry>();
	}

	public Version(String release, String version, ArrayList<Entry> entries) {
		super();
		this.release = release;
		this.version = version;
		this.entries = entries;
	}

	public String getRelease() {
		return this.release;
	}

	public String getVersion() {
		return this.version;
	}

	public ArrayList<Entry> getEntries() {
		return this.entries;
	}

	public void add(Entry entry) {
		this.entries.add(entry);
		System.out.println("Added " + entry.getEntry() + " to version.");
	}

}
