package de.opencharacter.utils;

import java.util.ArrayList;

public class Changelog {

	private ArrayList<Version> versions;

	public Changelog(ArrayList<Version> entries) {
		super();
		this.versions = entries;
	}

	public ArrayList<Version> getVersions() {
		return this.versions;
	}

	public void add(Version version) {
		this.versions.add(version);
		System.out.println("Added " + version.getRelease() + " to version.");
	}
}
